'use strict';

const http = require('http');
const debug = require('debug')('nodestr:server');
const app = require('../src/app');

const port = process.env.PORT || 8080;
app.set('port', port);

const server = http.createServer(app);
server.listen(port);

console.log("API rodando na porta "+port);
//debug(server);

//Views
/*console.log(__dirname + '../src/views');
app.set('views', __dirname + '../src/views'); 
app.set('view engine', 'html');
app.engine('html', createEngine());

console.log();*/