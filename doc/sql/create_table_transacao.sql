create table transacao(
	id int primary key auto_increment,
    id_usuario int,
    constraint fk_usuario_id foreign key(id_usuario)
    references usuario(id)
    on update cascade on delete restrict,
    hash_transacao varchar(150),
    chave_final varchar(300) not null
)Engine=InnoDB;