create table usuario (
	id int(11) NOT NULL AUTO_INCREMENT,
	nome varchar(100) DEFAULT NULL,
	carteira varchar(250) DEFAULT NULL,
	senha varchar(250) DEFAULT NULL,
	chave_publica varchar(500) DEFAULT NULL,
	PRIMARY KEY (id)
)ENGINE=InnoDB;
