const fs = require('fs');
const Crypto = require('crypto');
const AES256 = require('aes256');
const NodeRSA = require('node-rsa');
const BancoDados = require('./dao/bancoDados');
const Ethereum = require('./dao/ethereum');
const CHAVE_SISTEMA = "FLARTPMENGOLKJJ21396AKJ85DPOIUE8PULJaop789==";

class Mediador{

    constructor(){
        this.bancoDados = new BancoDados();
        this.ethereum = new Ethereum();
        //this.exp = new RegExp('#!!HEADER!!#(.*?)\/!!HEADER!!\/');
    }

    obterDataAtual(){
        var data = new Date();
        var ano = data.getFullYear();
        var mes = data.getMonth()+1;
        var dia = data.getDate();
        //var horario = data.getHours()+":"+data.getMinutes()+":"+data.getSeconds();
        var dataAtual = ano+"-"+mes+"-"+dia;
        return dataAtual;
    }

    buildTransaction(file, owner, user = null, deadline = "null", callback){
        try{
            let _this = this;

            var dataAtual = this.obterDataAtual();

            if (fs.existsSync(file)) {
                var header = {
                    owner: owner,
                    datetime: dataAtual,
                    deadline: deadline,
                    user: user === null ? owner : user
                };
            } else {
                // var header = {
                //     owner: owner,
                //     time: time,
                //     user: user === null ? owner : user
                // };
            }

            _this.buildDocument(header, file, callback);
        }catch(err){
            callback(err);
        }
    }

    buildDocument(header, file, callback) {
        try{
            let _this = this;

            //console.log(file);

            var conteudo = "";
            //var conteudo = '#!!HEADER!!#' + JSON.stringify(header) + '/!!HEADER!!/';

            fs.readFile(file, function(err, data) {
                if (err) throw err;
                conteudo += data;

                //console.log(conteudo);

                _this.cryptDocument(conteudo, header, file, callback);
            });
        }catch(err){
            callback(err);
        }
    }

    cryptDocument(document, header, file, callback) {
        try{
            let _this = this;

            var hash = Crypto.createHash('md5').update(document).digest('hex');

            //Criptografia do documento com a chave aleatória = Cript_document
            var chave_aleatoria = (Math.random()*100).toString();
            //console.log(chave_aleatoria);
            var documentEncryptedAES_random = AES256.encrypt(chave_aleatoria, document);
            //console.log(documentEncryptedAES_random);

            //Criptografia do Cript_document com a chave do sistema
            var documentEncryptedAES_sistema = AES256.encrypt(CHAVE_SISTEMA, documentEncryptedAES_random);
            //console.log(documentEncryptedAES_sistema);

            //Ordem de descriptografia (colocar na função "decryptDocument")
            /*var decryptedAES_sistema = AES256.decrypt(CHAVE_SISTEMA, documentEncryptedAES_sistema);
            console.log(decryptedAES_sistema);
            var decryptedAES_random = AES256.decrypt(chave_aleatoria, decryptedAES_sistema);
            console.log(decryptedAES_random);*/

            const dirCarteira = './carteiras/' + header.user; //no local
            //const dirCarteira = '/home/cefet/dlm-ethereum/carteiras/' + header.user; //no servidor
            const archive = dirCarteira + '/' + hash;
            header.dirCarteira = dirCarteira;
            header.archive = archive;

            if (!fs.existsSync(dirCarteira)){
                fs.mkdirSync(dirCarteira);
            }

            if (!fs.existsSync(archive)){
                fs.mkdirSync(archive);
            }

            //Criptografa a chave aleatória com a chave privada do usuário e, com a chave do sistema
            _this.cryptKey(chave_aleatoria, header, function(dados){
                //console.log(dados);
            
                var chaveFinal = dados.chaveFinal;
                //var publicKey = dados.publicKey;
                
                //Teste de Descriptografia
                //console.log(chave_aleatoria); //chave aleatória (sem criptografia)
                //console.log(dados);
                /*var chRSA = AES256.decrypt(CHAVE_SISTEMA, dados.chaveFinal);
                var key = new NodeRSA({b: 512});
                key.importKey(dados.publicKey, 'pkcs8-public-pem');
                var chRandom = key.decryptPublic(chRSA, 'utf8');*/
                //console.log(chRandom); //chave aleatória (depois das descriptografias)


                fs.writeFile(archive+ '/'+ header.user + '.txt', documentEncryptedAES_sistema, function (err, data) {

                    if (err) throw err;
                    console.log('Arquivo encriptado salvo!');

                    _this.ethereum.registerTransaction(hash, header, file, function(resposta){
                        resposta.chaveFinal = chaveFinal;
                        callback(resposta);
                    });
                });    
            });            

        }catch(err){
            callback(err);
        }
    }

    gerarParDeChavesPublicaPrivada(carteiraUsuario, dirCarteira, callback){
        try{
            let _this = this;

            _this.obterChavePublica(carteiraUsuario, function(chavePublica){

                if(chavePublica == undefined || chavePublica == null){
                    var arquivoChavePrivada = dirCarteira+"/"+carteiraUsuario+'.pem';
                    //var arquivoChavePublica = dirCarteira +'/pub-'+carteiraUsuario+'.pem';

                    if (!fs.existsSync(dirCarteira)){
                        fs.mkdirSync(dirCarteira);
                    }

                    if(!fs.existsSync(arquivoChavePrivada)){
                        var key = new NodeRSA({b: 512});
                        var privateKey = key.exportKey('pkcs1-private-pem');
                        var publicKey = key.exportKey('pkcs8-public-pem');

                        _this.bancoDados.salvarChavePublicaUsuario(carteiraUsuario, publicKey, function(response){
                            if(response.affectedRows == 1){
                                console.log('Chave pública salva!');
                                fs.writeFile(arquivoChavePrivada, privateKey, function (err, data) {
                                    if (err) throw err;
                                    console.log('Chave privada salva!');
                                    callback(true);
                                    /*fs.writeFile(arquivoChavePublica, publicKey, function (err, data) {
                                        if (err) throw err;
                                        console.log('Chave pública salva!');
                                        callback(true);
                                    });*/
                                });
                            }else{
                                throw ("Não foi possível salvar a chave pública do usuário no banco de dados");
                            }
                        });
                    }else{
                        throw ("O usuário já possui sua chave privada!");
                    }
                }else{
                    console.log('O par de chaves já foi gerado para o usuário!');
                    callback(true);
                }
            });
        }catch(err){
            callback(err);
        }
    }

    //Processo de criptografia das chaves
    //@return: retorna a chaveFinal (da última criptografia) e a publicKey
    cryptKey(chaveAleatoria, header, callback){
        var carteiraUsuario = header.user;
        var dirCarteira = header.dirCarteira;

        this.gerarParDeChavesPublicaPrivada(carteiraUsuario, dirCarteira, function(resposta){
            //criptografar a chave aleatoria via RSA (com a chave privada)
            //Criptografia RSA (da chave aleatória)
            var arquivoPrivateKey = dirCarteira+'/'+carteiraUsuario+'.pem';
            fs.readFile(arquivoPrivateKey, function(err, privateKey){
                if(err) throw (err);

                /*this.bancoDados.obterUsuario(carteiraUsuario, function(usuario){
                    var publicKey = usuario[0].chave_publica;*/

                    var key = new NodeRSA({b: 512});
                    key.importKey(privateKey, 'pkcs1-private-pem');

                    var encryptedChaveAleatoria = key.encryptPrivate(chaveAleatoria, 'base64');
                    //console.log(encryptedChaveAleatoria);

                    //Decriptando com RSA (chave pública)
                    /*var decryptedChaveAleatoria = key.decryptPublic(encryptedChaveAleatoria, 'utf8');
                    console.log(decryptedChaveAleatoria);*/

                    //O resultado da criptografia RSA será criptografado via AES com a chave do sistema
                    var chaveFinal = AES256.encrypt(CHAVE_SISTEMA, encryptedChaveAleatoria);

                    var dados = {
                        "chaveFinal": chaveFinal/*,
                        "publicKey": publicKey*/
                    }

                    callback(dados);
                //});
            });
        });        
    }

    //Descriptografia do arquivo
    /*decryptDocument(carteiraUsuario, hashTransacao, arquivoEncriptado, callback){
        this.bancoDados.obterTransacao(carteiraUsuario, hashTransacao, function(transacao){
            if(transacao != null && transacao != "" && transacao != undefined){
                var chaveFinal = transacao[0].chave_final;
                if(chaveFinal != null && chaveFinal != "" && chaveFinal != undefined){
                    //Decriptando a chave final com a chave do sistema (resultando em: chRSA)
                    var chRSA = AES256.decrypt(CHAVE_SISTEMA, chaveFinal);

                    this.bancoDados.obterUsuario(carteiraUsuario, function(usuario){
                        //Decriptando a chave chRSA (decriptada anteriormente) com a chave pública do usuário 
                        var publicKey = usuario[0].chave_publica;
                        var key = new NodeRSA({b: 512});
                        key.importKey(publicKey, 'pkcs8-public-pem');
                        var chaveAleatoria = key.decryptPublic(chRSA, 'utf8');

                        //Arquivo decriptado com a chave do sistema
                        var decryptedAES_sistema = AES256.decrypt(CHAVE_SISTEMA, arquivoEncriptado);
                        //Arquivo decriptado com a chave aleatória
                        var arquivoDecriptado = AES256.decrypt(chaveAleatoria, decryptedAES_sistema);
                        //console.log(decryptedAES_random);
                        var resposta = {
                            status: true,
                            message: arquivoDecriptado
                        }
                        callback(resposta);
                    });
                }else{
                    var resposta = {
                        status: false,
                        message: "Não foi possível decriptar o arquivo!"
                    }
                    callback(resposta);
                }
            }else{
                var resposta = {
                    status: false,
                    message: "Arquivo não encontrado para decriptação!"
                }
                callback(resposta);
            }            
        });   
    }*/

    /*getHeader(file, key = null) {
        if ( key === null ) {
            // Chamar função para tentar pegar chave
            return false;
        }

        return new Promise((resolve, reject) => {
            fs.readFile(key, function(err, privateKey) {
              if (err) throw err;

              privateKey = privateKey.toString('utf8');

              const secureKey = new NodeRSA();
              secureKey.importKey(privateKey);

              return fs.readFile(file, function(err, encrypted) {
                  if (err) throw err;

                  encrypted = encrypted.toString('utf8');

                  try {
                      const decrypted = secureKey.decrypt(encrypted, 'utf8');
                      header = exp.exec(decrypted)[0].replace('#!!HEADER!!#', '').replace('/!!HEADER!!/', '');
                      resolve(header);
                  } catch (error) {
                      privateKey = null;
                      destroyFile(key);
                      reject(false);
                  }
              });
            });
        });
    }

    validateUser(file, key, user) {
        let _this = this;
        return new Promise((resolve, reject) => {
            _this.getHeader(file, key).then(function(res){
              var header = JSON.parse(res);

              resolve(header.user === user);
            });
        });
    }

    readFile(file, key, user) {
        let _this = this;
        _this.validateUser(file, key, user).then(function(res) {
          if ( !res ) {
            destroyFile(key);
            return false;

          } else {
            fs.readFile(key, function(err, privateKey) {
                if (err) throw err;

                privateKey = privateKey.toString('utf8');

                const secureKey = new NodeRSA();
                secureKey.importKey(privateKey);

                fs.readFile(file, function(err, encrypted) {
                    if (err) throw err;

                    encrypted = encrypted.toString('utf8');

                    let decrypted = secureKey.decrypt(encrypted, 'utf8');
                    decrypted = decrypted.replace(exp.exec(decrypted)[0], '');

                    console.log(decrypted);
                });
            });
          }
        });
    }

    destroyFile(key) {
        fs.unlink(key, function (err) {
            if (err) throw err;
            console.log('Chave deletada!');
        });
    }*/

    obterObra(hashTransacao, callback){
        let _this = this;
        _this.ethereum.obterObra(hashTransacao, callback);
    }

    obterObras(callback){
        let _this = this;
        _this.ethereum.obterObras(callback);
    }

    cadastrarUsuario(nomeUsuario, carteiraUsuario, senhaUsuario, callback){
        let _this = this;
        _this.ethereum.cadastrarUsuario(nomeUsuario, carteiraUsuario, senhaUsuario, callback);
    }

    transferirEther(usuarioOrigem, usuarioDestino, valorEther, callback){
        let _this = this;
        _this.ethereum.transferirEther(usuarioOrigem, usuarioDestino, valorEther, callback);
    }

    obterTransacoesDoUsuario(carteiraUsuario, callback){
        let _this = this;
        _this.ethereum.obterTransacoesDoUsuario(carteiraUsuario, callback);
    }

    obterTransacaoUsuario(carteiraUsuario, hashTransacao, callback){
        this.bancoDados.obterTransacaoUsuario(carteiraUsuario, hashTransacao, callback);
    }

    obterChavePublica(carteiraUsuario, callback){
        this.bancoDados.obterChavePublica(carteiraUsuario, callback);
    }

    validarPermissaoAcesso(carteiraUsuario, hashTransacao, callback){
        this.obterObra(hashTransacao, function(result){
            if(result == null){
                var permissao = {
                    'hashObra': null,
                    'permissao': false
                };
            }else{
                var cabecalhoObra = result;
                var owner = cabecalhoObra[1];
                var user = cabecalhoObra[4];
                var hashObra = cabecalhoObra[0];
                var deadline = cabecalhoObra[2]; //validade empréstimo

                var partesDeadline = deadline.split(" ");
                var dataDeadline = partesDeadline[0];
                var partesDataDeadline = dataDeadline.split("-");

                var dataValidade = new Date(partesDataDeadline[0], partesDataDeadline[1] - 1, partesDataDeadline[2]);
                var dataAtual = new Date();

                //console.log(user +"--------"+ carteiraUsuario);
                if(user == carteiraUsuario){

                    //Validando a data de validade do empréstimo
                    if(dataAtual > dataValidade){
                        var permissao = {
                            'hashObra': null,
                            'permissao': false
                        };

                        if(user != owner){
                            console.log("A obra será retornada para o seu owner");
                            //chamar a função para transferir obra para o owner novamente
                        }
                    }else{
                        var permissao = {
                            'hashObra': hashObra,
                            'owner': owner,
                            'permissao': true
                        };
                    }
                }else{
                    var permissao = {
                        'hashObra': null,
                        'permissao': false
                    };
                }
            }

            callback(permissao);
        });
    }
}

module.exports = Mediador;