'use strict';

const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const path = require('path'); //não estou usando por enquanto
const bodyParser = require('body-parser'); //não estou usando por enquanto  
const cors = require('cors');
const app = express();

//Cors
app.use(cors());

//Carregar as Rotas

//ROTA: sobre obras
const obraRoute = require('./routes/obra-route');
app.use("/", obraRoute);

const usuarioRoute = require('./routes/usuario-route');
app.use("/", usuarioRoute);

//View
app.set('views', __dirname+'\/views');
app.set('view engine', 'ejs');    // Setamos que nossa engine será o ejs
app.use(expressLayouts);         // Definimos que vamos utilizar o express-ejs-layouts na nossa aplicação
app.use(bodyParser.urlencoded({ extended: false })); 	// Com essa configuração, vamos conseguir parsear o corpo das requisições
app.use(bodyParser.json());



module.exports = app;