'use strict';

const fs = require('fs');
const crypto = require('crypto');
const formidable = require('formidable');
const ContractInstance = require('../dao/ethereum');
const BancoDados = require('../dao/bancoDados');
const Mediador = require('../mediador');

class ObraController{

	constructor(){
		//this.contractInstance = new ContractInstance();
		this.bancoDados = new BancoDados();
		this.mediador = new Mediador();
	}

	//obter todas as obras cadastradas
	/*obterObras = (req, res, next) => {
		contractInstance.methods.obterTodasAsObras().call({from: "0x074A2E6CFc35C84F386b975E1eb2749bE2570219"}).then(function(response){
			var responseEmAscii = [];
			for(var i=0; i< response.length; i++){
				responseEmAscii[i] = contractInstance.utils.toAscii(response[i]);
			}
			res.status(200).send({
				response: response,
				error: null
			});
		}).catch(function(error){
			res.status(400).send({
				response: "Houve um erro! Verifique sua conexão com a EVM",
				error: "Detalhes - "+error
			});
		});
	}*/

	obterObra(req, res, next) {
		var hashTransacao = req.params.hashTransacao;
		this.mediador.obterObra(hashTransacao, function(result){
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "O registro da obra não foi encontrado!",
					error: true
				});
			}
		});
	}

	obterObras(req, res, next) {
		this.mediador.obterObras(function(result){
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "Houve um erro ao obter todas as obras",
					error: true
				});
			}
		});
	}

	viewCadastrarObra(req, res, next){
		res.render("cadastro-obra", {usuario:'Henrique Júnior'});
	}

	//Cadastrar obras na EVM
	cadastrarObra(req, res, next) {
	    let _this = this;
	  	var form = new formidable.IncomingForm();
	    form.parse(req, function(err, fields, files){
	    	try{

		      	//console.log(fields); //json com os campos do formulário
		      	//console.log(files.arquivo.size);//size do arquivo
				if(files.arquivo == undefined)
					throw new Error("Arquivo inexistente!");
				if(files.arquivo.size <= 0)
					throw new Error("Arquivo não foi inserido!");
				if(fields.carteiraMediador.length <= 0)
					throw new Error("Insira a carteira do mediador!");

				var oldpath = files.arquivo.path;
			    //var newpath = '/home/cefet/dlm-ethereum/storage/'+files.arquivo.name;
			    var newpath = './storage/'+files.arquivo.name;
			    fs.rename(oldpath, newpath, function(err){
			        if(err){
			        	throw new Error(err);
			        }else{
			        	//Criar o cabeçalho do arquivo
			        	//Adicionar o cabeçalho do arquivo na EVM
			        	//Criptografar o arquivo e inserir este novo arquivo em outra pasta

			        	//TESTANDO
			        	//var carteiraMediador = "0x804627Db188c0fc706b62DaFF3Ad8CF1169b95F4";
			        	var carteiraMediador = fields.carteiraMediador;
			        	var deadline = "null";

			        	//OBS.: validar a carteira do mediador! Antes de enviar para a buildTransaction

			        	_this.mediador.buildTransaction(newpath, carteiraMediador, null, deadline, function(resposta){
			        		//console.log(resposta);
			        		if(resposta.status == true){
			        			var transactionHash = resposta.transactionHash;
			        			var chaveFinal = resposta.chaveFinal;
						        
						        _this.bancoDados.salvarTransacao(transactionHash, carteiraMediador, chaveFinal, function(response){
									console.log(response);
									if(response.affectedRows == 1){
										res.status(200).send({
											response: "Arquivo salvo com sucesso! Hash da transação: "+transactionHash,
											error: err
										});
									}else{
										res.status(400).send({
											response: "Houve um erro ao registrar a obra no banco de dados!",
											error: response.message
										});
									}
								});
			        		}else{
			        			var mensagemError = "";
			        			if(resposta.logs != undefined){
			        				logsError = resposta.logs;
			        				mensagemError = logsError.join(" - ");
			        			}else if(resposta != undefined){
			        				mensagemError = "Detalhes - "+resposta;
			        			}

			        			res.status(400).send({
									response: "Houve um erro ao registrar a obra!",
									error: mensagemError
								});
			        		}
			        	});     	
			        }
		      	});
			}catch(err){
				res.status(400).send({
					response: "Houve um problema ao salvar o arquivo!",
					error: "Detalhes - "+err
				});
			}
	    });
	}

	//Cadastrar obras na EVM - OBS.: TERMINAR DE FAZER!
	transferirObra(req, res, next){
    	let _this = this;
		var hashTransacao = req.params.hashTransacao;
  		var carteiraUsuarioOrigem = req.params.carteiraUsuarioOrigem;
		var carteiraUsuarioDestino = req.params.carteiraUsuarioDestino;
  		var validade = req.params.validade; //se for igual a "NULL" então será uma transferência permanente
  		//console.log(req.params);
  		
    	try{
    		_this.mediador.validarPermissaoAcesso(carteiraUsuarioOrigem, hashTransacao, function(resposta){
				//console.log(resposta);
				var caminhoDestino = './storage/';
				//Obtem os arquivos originais salvos no servidor
				//Lendo todos os arquivos existenstes na pasta storage de forma síncrona
				fs.readdirSync(caminhoDestino).forEach(file => {
					//Efetuando a leitura do arquivo
					fs.readFile(caminhoDestino+file,'utf8', function(err,data){
						//Enviando para o console o resultado da leitura
						if (err) throw err;
			                var conteudo = data;

			                //console.log(conteudo);
							var hash = crypto.createHash('md5').update(conteudo).digest('hex');
							console.log(hash +"--------------"+resposta.hashObra);
							if(hash == resposta.hashObra){
								var caminhoDestinoCorreto = caminhoDestino+file;
								if(resposta.permissao == true && caminhoDestinoCorreto != "" && caminhoDestinoCorreto != undefined && caminhoDestinoCorreto != null){
									_this.mediador.buildTransaction(caminhoDestinoCorreto, resposta.owner, carteiraUsuarioDestino, validade, function(resposta){
						        		if(resposta.status == true){
						        			var transactionHash = resposta.transactionHash;
						        			var chaveFinal = resposta.chaveFinal;
									        
									        _this.bancoDados.salvarTransacao(transactionHash, carteiraUsuarioDestino, chaveFinal, function(response){
									        	console.log(response);
												if(response.affectedRows == 1){
													res.status(200).send({
														response: "Arquivo salvo com sucesso! Hash da transação: "+transactionHash,
														error: false
													});
												}else{
													res.status(400).send({
														response: "Houve um erro ao registrar a obra no banco de dados!",
														error: response.message
													});
												}
											});
						        		}else{
						        			var mensagemError = "";
						        			if(resposta.logs != undefined){
						        				logsError = resposta.logs;
						        				mensagemError = logsError.join(" - ");
						        			}else if(resposta != undefined){
						        				mensagemError = "Detalhes - "+resposta;
						        			}

						        			res.status(400).send({
												response: "Houve um erro ao transferir a obra!",
												error: mensagemError
											});
						        		}
						        	});
								}else{
									res.status(400).send({
										response: "Houve um erro ao transferir a obra! O usuário origem não tem permissão de acesso a obra ou o caminho do arquivo original não existe!",
										error: true
									});
								}
							}
					});
				});	
    		});
		        	
		}catch(err){
			res.status(400).send({
				response: "Houve um problema ao transferir o arquivo!",
				error: "Detalhes - "+err
			});
		}
	}

}

module.exports = ObraController;