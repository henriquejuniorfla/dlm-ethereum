'use strict';

const fs = require('fs');
const formidable = require('formidable');
//const contractInstance = require('../dao/ethereum');
const Mediador = require('../mediador');

class UsuarioController{

	constructor(){
		this.mediador = new Mediador();
	}

	/*obterUsuarios(req, res, next){
		contractInstance.methods.obterTodasAsObras().call({from: "0x074A2E6CFc35C84F386b975E1eb2749bE2570219"}).then(function(response){
			var responseEmAscii = [];
			for(var i=0; i< response.length; i++){
				responseEmAscii[i] = contractInstance.utils.toAscii(response[i]);
			}
			res.status(200).send({
				response: response,
				error: null
			});
		}).catch(function(error){
			res.status(400).send({
				response: "Houve um erro! Verifique sua conexão com a EVM",
				error: "Detalhes - "+error
			});
		});
	}*/

	viewCadastrarUsuario(req, res, next){
		res.render("cadastro-usuario", {usuario:'Henrique Júnior'});
	}

	viewTransferirEther(req, res, next){
		res.render("transferir-ether-usuario");
	}

	cadastrarUsuario(req, res, next){
		let _this = this;
	  	var form = new formidable.IncomingForm();
	    form.parse(req, function(err, fields, files){
	    	try{
		      	//console.log(fields); //json com os campos do formulário
		      	//console.log(files.arquivo.size);//size do arquivo
		      	var nomeUsuario = fields.nome;
		      	var carteiraUsuario = fields.carteiraUsuario;
		      	var senhaUsuario = fields.senha;

		      	if(nomeUsuario.length <= 0)
					throw new Error("Insira o nome do usuário!");
				if(carteiraUsuario.length <= 0)
					throw new Error("Insira a carteira do usuário!");
				if(senhaUsuario.length <= 0)
					throw new Error("Insira a senha do usuário!");

				_this.mediador.cadastrarUsuario(nomeUsuario, carteiraUsuario, senhaUsuario, function(resposta){
					console.log(resposta);
					if(resposta.serverStatus == 2){
						res.status(200).send({
							response: "Usuário "+nomeUsuario+" cadastrado com sucesso!",
							error: err
						});
					}else{
						throw new Error(resposta.message);
					}
				});
			}catch(err){
				res.status(400).send({
					response: "Houve um problema ao cadastrar o usuário!",
					error: "Detalhes - "+err
				});
			}
	    });
	}

	transferirEther(req, res, next){
		let _this = this;
	  	var form = new formidable.IncomingForm();
	    form.parse(req, function(err, fields, files){
	    	try{
		      	//Para transferência de Ether
		      	var usuarioOrigem = fields.usuarioOrigem;
		      	var usuarioDestino = fields.usuarioDestino;
		      	var valorEther = fields.valorEther;

		      	if(usuarioOrigem.length <= 0)
					throw new Error("Insira a carteira do usuário origem!");
				if(usuarioDestino.length <= 0)
					throw new Error("Insira a carteira do usuário destino!");
				if(valorEther <= 0)
					throw new Error("Insira um valor de ether maior que zero!");

				_this.mediador.transferirEther(usuarioOrigem, usuarioDestino, valorEther, function(resposta){
					//console.log(resposta);
					if(resposta.status == true){
						res.status(200).send({
							response: "Foi transferido "+valorEther+" ether de "+usuarioOrigem+" para "+usuarioDestino,
							error: err
						});
					}else{
						throw new Error(resposta.message);
					}
				});
			}catch(err){
				res.status(400).send({
					response: "Houve um problema durante a transferência de ether!",
					error: "Detalhes - "+err
				});
			}
	    });
	}

	visualizarObra(req, res, next){
		try{
			let _this = this;
			var carteiraUsuario = req.params.carteiraUsuario;
			var hashTransacao = req.params.hashTransacao;

			_this.mediador.validarPermissaoAcesso(carteiraUsuario, hashTransacao, function(result){
				if(result.permissao == true && result.hashObra != null){
					var arquivo = './carteiras/' + carteiraUsuario + '/' + result.hashObra + '/' + carteiraUsuario + '.txt';
					fs.readFile(arquivo, function(err, encrypted) {
	                    if (err) throw err;

	                    var arquivoEncriptado = encrypted.toString('utf8');
			            res.status(200).send({
							response: arquivoEncriptado,
							error: false
						});
	                });
				}else{
					res.status(400).send({
						response: "O usuário "+carteiraUsuario+" não tem permissão de acesso a obra solicitada!",
						error: true
					});
				}
			});
		}catch(err){
			res.status(400).send({
				response: "Houve um problema ao tentar visualizar a obra! Detalhes - "+err,
				error: true
			});
		}
		
	}

	obterTransacoesDoUsuario(req, res, next){
		let _this = this;
		var carteiraUsuario = req.params.carteiraUsuario;

		//buscar os registros das transações para o usuário
		_this.mediador.obterTransacoesDoUsuario(carteiraUsuario, function(result){
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "As transações do usuário não foram encontradas!",
					error: true
				});
			}
		});

		/*var hashTransacao = req.params.hashTransacao;
		_this.mediador.obterObra(hashTransacao, function(result){
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "O registro da obra não foi encontrado!",
					error: true
				});
			}
		});*/
	}

	obterChavePublica(req, res, next){
		let _this = this;
		var carteiraUsuario = req.params.carteiraUsuario;

		_this.mediador.obterChavePublica(carteiraUsuario, function(result){
			console.log(result);
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "Não foi possível obter a chave pública do usuário!",
					error: true
				});
			}
		});
	}

	obterTransacaoUsuario(req, res, next){
		let _this = this;
		var carteiraUsuario = req.params.carteiraUsuario;
		var hashTransacao = req.params.hashTransacao;

		_this.mediador.obterTransacaoUsuario(carteiraUsuario, hashTransacao, function(result){
			if(result != null){
				res.status(200).send({
					response: result,
					error: null
				});
			}else{
				res.status(400).send({
					response: "As transações do usuário não foram encontradas!",
					error: true
				});
			}
		});
	}

}

module.exports = UsuarioController;