
class BancoDados{

	constructor(){
		this.con = this.conectar();
	}

	conectar(){
		var mysql = require("mysql");
		var con = mysql.createConnection({
		  	host: "localhost",
		  	/*user: "cefet",
		  	password: "C3F3T@cefet",*/
		  	user: "root",
		  	password: "root",
		  	database: "dlm"
		});
		con.connect();
		return con;
	}

	cadastrarUsuario(nome, carteiraUsuario, senha, callback){
		try{
			/*this.con.connect(function(err) {
			  	if(err) throw err;*/
			let _this = this;

			this.existeUsuario(carteiraUsuario, function(response){
				var idUsuario = response;
				if(idUsuario != null){
					throw "Usuário já existente no cadastro de usuários do banco de dados DLM! Id do usuário:"+idUsuario;
				}else{
					var sql = "insert into usuario(nome, carteira, senha) values ? ;";
					var values = [
						[nome, carteiraUsuario, senha]
					];
					_this.con.query(sql, [values], function (err, result) {
					  	if (err) throw err;
					   	callback(result);
					});
				}
			});
		}catch(err){
			callback(err);
		}
	}

	obterUsuarios(){
		/*this.con.connect(function(err) {
		  	if (err) throw err;*/
		  	let _this = this;

		  	var sql = "select * from usuario;";
		  	_this.con.query(sql, function (err, result) {
		    	if (err) throw err;
		    	console.log(result);
		  	});
		//})
	}

	obterUsuario(carteiraUsuario, callback){
		/*this.con.connect(function(err) {
		  	if (err) throw err;*/
		  	let _this = this;

		  	var sql = "select * from usuario where carteira = '"+carteiraUsuario+"';";
		  	_this.con.query(sql, function (err, result) {
		    	if (err) throw err;
		    	callback(result);
		  	});
		//})
	}

	salvarChavePublicaUsuario(carteiraUsuario, chavePublica, callback){
		  	let _this = this;

		  	var sql = "update usuario set chave_publica = '"+chavePublica+"' where carteira = '"+carteiraUsuario+"' and (chave_publica = '' or chave_publica is null);";
		  	_this.con.query(sql, function (err, result) {
		    	if (err) throw err;
		    	callback(result);
		  	});
	}

	obterChavePublica(carteiraUsuario, callback){
		let _this = this;

		var sql = "select chave_publica from usuario where carteira = '"+carteiraUsuario+"';";
		_this.con.query(sql, function (err, result) {
		   	if (err) throw err;
		   	callback(result[0].chave_publica);
		});
	}

	salvarTransacao(hashTransacao, carteiraUsuario, chaveFinal, callback){
		try{
			let _this = this;

			this.existeUsuario(carteiraUsuario, function(response){
				var idUsuario = response;
				if(idUsuario == null){
					throw new Error("Usuário inexistente no cadastro de usuários do banco de dados DLM!");
				}else{
					var sql = "insert into transacao(hash_transacao, id_usuario, chave_final) values ? ;";
					var values = [
					  	[hashTransacao, idUsuario, chaveFinal]
					];

					_this.con.query(sql, [values], function (err, result) {
					   	if (err) throw err;
					   	callback(result);
					});
				}
			});
		}catch(err){
			callback(err);
		}
	}

	existeUsuario(carteiraUsuario, callback){
		this.obterUsuario(carteiraUsuario, function(response){
			var usuario = response[0];
			//console.log(usuario);
			if(usuario == undefined){
				var resposta = null;
			}else{
				var idUsuario = usuario.id;
				var resposta = idUsuario;
			}
			callback(resposta);
		});
	}

	obterTransacaoUsuario(carteiraUsuario, hashTransacao, callback){
		let _this = this;
		_this.obterUsuario(carteiraUsuario, function(usuario){
			var idUsuario = usuario[0].id;

			var sql = "select * from transacao where id_usuario = "+idUsuario+" and hash_transacao = '"+hashTransacao+"';";
			console.log(sql);
			_this.con.query(sql, function (err, result) {
			   	if (err) throw err;
			   	callback(result);
			});
		});

	}

	obterTransacoesDoUsuario(carteiraUsuario, callback){
		try{
			let _this = this;
			//console.log(_this.con.state);
			//_this.con.connect(function(err) {
				//if(err) throw err;

				var sql = "select transacao.hash_transacao from usuario join transacao on(usuario.id = transacao.id_usuario) where usuario.carteira = '"+carteiraUsuario+"';";
				_this.con.query(sql, function (err, result) {
			    	if (err) throw err;
			    	callback(result);
			  	});
			//});
		}catch(err){
			callback(err);
		}
	}


}

module.exports = BancoDados;