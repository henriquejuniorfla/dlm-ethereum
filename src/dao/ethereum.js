'use strict';
const Web3 = require('web3');
const BancoDados = require('../dao/bancoDados');

class Ethereum{

	constructor(){
		this.web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:7545")); //Local
		//this.web3 = new Web3(new Web3.providers.HttpProvider("http://192.168.67.95:7545")); //Server Blockchain
		this.contractInstance = this.interfaceEthereum();
		this.bancoDados = new BancoDados();
	}

	interfaceEthereum(){
		if(this.web3 != null){
			var conexaoEthereum = this.web3.eth.ens._currentProvider.connected;
			if(conexaoEthereum == true){
				var abi = this.obterABI();
				var addressContract = "0xB7027dd86FA0F5758B45910330006Fb5446e2941"; //Local
				//var addressContract = "0x50F71bD52B1b9717B83EB0AE384D3B4AB81E6431"; //Server Blockchain
				var contractInstance = new this.web3.eth.Contract(abi, addressContract);
				return contractInstance;
			}else{
				console.log("EVM Ethereum não conectado!");
			}
		}else{
			console.log("Houve um erro na conexão com a EVM Ethereum!");
		}
		return null;
	}

	obterABI(){
		var contratoJSON = require("../../node_modules/truffle/build/contracts/DLMContract.json");
		var abi = contratoJSON.abi;
		return abi;
	}

	//Registra Transação na Ethereum (OBS.: esse registro será apenas para Mediador)
    registerTransaction(hash, header, file, callback){
    	try{
	    	var web3 = this.web3;
	    	var DLMContract = this.contractInstance.methods;

	    	var remetente = header.owner;
	    	if(header.user != header.owner){
	    		remetente = header.user;
	    	}

	    	//var senhaUsuario = "senha123456789"; //teste (senha está no BD)
	    	//this.web3.eth.personal.unlockAccount(remetente, senhaUsuario, 600, function(resultado){
		    	DLMContract.setObra(
		    		hash, 
		    		header.owner,
		    		header.deadline, 
		    		header.datetime,  
		    		header.user
		    	).send({	
		    			from: remetente, 
		    			gasPrice: "20000000000", 
		    			gas: "6721975"
		    	}, function(err, response){
		    		if(err){
		    			throw (err);
		    		}

		    		var resposta = {
		    			status: true,
		    			transactionHash: response
		    		}
	    			callback(resposta);
				});
	    	//});


	        /*const archive = './files/' + header.user + '/' + hash;

	        var dadosObj = {
	            hash: hash,
	            owner: header.owner,
	            oldHash: header.oldHash,
	            time: header.time,
	            user: header.user,
	            key: header.key,
	            file: archive+".txt"
	        };

	        var dados = JSON.stringify(dadosObj);
	        var dadosHex = web3.utils.toHex(dados);
	        //var dadosString = web3.utils.toAscii(dadosHex);

	        var transactionOptions = {
	            from: dadosObj.user, //carteira do remetente
	            gasPrice: "20000000000",
	            gas: "6721975",
	            to: dadosObj.user, //carteira do destinatário
	            value: "1000",
	            data: dadosHex
	        };

	        web3.eth.sendTransaction(transactionOptions).then(function(response){
	            //console.log(response.transactionHash);
	            callback(response);
	        }).catch(function(err){
	            //OBS.: está dando o erro "Transaction has been reverted by the EVM" no console
	            //Pelos comentários, o Ganache está com esse problema
	            //Por isso, foi feita esta gambiarra (rsrs)
	            var errorString = err.message;
	            var resultado = errorString.split("{");
	            resultado = resultado[1].split("}");
	            resultado = '{'+resultado[0]+'}';
	            var resultadoJSON = JSON.parse(resultado);
	            console.log(resultadoJSON.transactionHash);
	            callback(resultadoJSON);
	        });*/
    	}catch(err){
    		callback(err);
    	}
    }

    //Obtém o registro da transação através do hash da transação
    obterObra(hashTransacao, callback){
    	try{
	    	var web3 = this.web3;
	    	var DLMContract = this.contractInstance.methods;

    		web3.eth.getTransaction(hashTransacao).then(function(response){
    			//console.log(response);
    			if(response == null){
    				callback(null);
    			}else{
    				var dadosTransacao = web3.utils.hexToAscii(response.input);
	    			dadosTransacao = dadosTransacao.split("*");
	    			var hashObra = dadosTransacao[0].split(" ", 2);
	    			hashObra = (hashObra[1]).toString();
	    			hashObra = hashObra.substring(0, 32);

	    			DLMContract.getObra(hashObra).call({}, function(err, result){
	    				//console.log(result);
	    				if(err){
	    					throw (err);
	    				}else{
	    					callback(result);
	    				}
	    			});
    			}
    		});
    	}catch(err){
    		callback(err);
    	}
    }

    obterObras(callback){
    	try{
	    	var web3 = this.web3;
	    	var DLMContract = this.contractInstance.methods;

	    	DLMContract.getObras().call({from: ''}, function(err, result){
    			if(err){
    				throw (err);
    			}else{
    				callback(result);
    			}
			});

    	}catch(err){
    		callback(err);
    	}
    }

    cadastrarUsuario(nomeUsuario, carteiraUsuario, senhaUsuario, callback){
    	try{
    		var web3 = this.web3;

    		//Cria novo usuário na Ethereum e retorna a carteira do usuário
    		/*web3.eth.personal.newAccount(senhaUsuario, function(err, result){
    			if(err){
    				throw (err);
    			}else{
    				var carteiraUsuario = result;
    				var chavePublica = "teseteChavepublica"; //gerar a chave pública
    				this.bancoDados.cadastrarUsuario(nomeUsuario, cpfUsuario, senhaUsuario, carteiraUsuario, chavePublica, function(resposta){
    					callback(resposta);
    				});
    			}
    		});*/
    		
    		this.bancoDados.cadastrarUsuario(nomeUsuario, carteiraUsuario, senhaUsuario, function(resposta){
    			callback(resposta);
    		});
    	}catch(err){
    		callback(err);
    	}
    }

    transferirEther(usuarioOrigem, usuarioDestino, valorEther, callback){
    	try{
    		var web3 = this.web3;

    		this.bancoDados.obterUsuario(usuarioDestino, function(resultado){
    			//console.log(resultado[0].senha);
    			var senhaUsuarioDestino = resultado[0].senha;
    			if(senhaUsuarioDestino != ""){
    				//Desbloqueia a conta criada (que não está na EVM Ganache)
    				web3.eth.personal.unlockAccount(usuarioOrigem, senhaUsuarioDestino, 1500)
						.then(console.log('Account unlocked!'));

					//Realiza a transferência 
					web3.eth.sendTransaction({from: usuarioOrigem, to: usuarioDestino, value: web3.utils.toWei(valorEther, 'ether')}).then(function(resposta){
						console.log(resposta);
						var resultado = {
							response: resposta,
							status: true
						}
						callback(resultado);
					});
    			}else{
    				throw new Error("Senha incorreta do usuário destino! Não foi possível desbloquear a conta!");
    			}
    		});
    	}catch(err){
    		callback(err);
    	}
    }

    obterTransacoesDoUsuario(carteiraUsuario, callback){
        this.bancoDados.obterTransacoesDoUsuario(carteiraUsuario, callback);
    }
}

module.exports = Ethereum;