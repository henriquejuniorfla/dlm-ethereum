
const express = require('express');
const router = express.Router();
const ObraController = require('../controllers/obra-controller');
var obraController = new ObraController();

router.get('/obras', function(req, res, next){
	obraController.obterObras(req, res, next);
});

router.get('/view/obra/cadastrar', function(req, res, next){
	obraController.viewCadastrarObra(req, res, next);
});

router.get('/obra/:hashTransacao', function(req, res, next){
	obraController.obterObra(req, res, next);
});

router.get('/obra/:hashTransacao/carteiraUsuarioOrigem/:carteiraUsuarioOrigem/carteiraUsuarioDestino/:carteiraUsuarioDestino/validade/:validade', function(req, res, next){
	obraController.transferirObra(req, res, next);
});

router.post('/obra/cadastrar', function(req, res, next){
	obraController.cadastrarObra(req, res, next);
});

module.exports = router;