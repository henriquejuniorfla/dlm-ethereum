'use strict';

const express = require('express');
const router = express.Router();
const UsuarioController = require('../controllers/usuario-controller');
const usuarioController = new UsuarioController();

//router.get('/usuarios', controller.obterUsuarios);

router.get('/view/usuario/cadastrar', function(req, res, next){
	usuarioController.viewCadastrarUsuario(req, res, next);
});

router.post('/usuario/cadastrar', function(req, res, next){
	usuarioController.cadastrarUsuario(req, res, next);
});

router.get('/view/usuario/transferir-ether', function(req, res, next){
	usuarioController.viewTransferirEther(req, res, next);
});

router.post('/usuario/transferir-ether', function(req, res, next){
	usuarioController.transferirEther(req, res, next);
});

router.get('/transacoes/usuario/:carteiraUsuario', function(req, res, next){
	usuarioController.obterTransacoesDoUsuario(req, res, next);
});

router.get('/usuario/:carteiraUsuario/publicKey', function(req, res, next){
	usuarioController.obterChavePublica(req, res, next);
});

router.get('/transacao/usuario/:carteiraUsuario/obra/:hashTransacao', function(req, res, next){
	usuarioController.obterTransacaoUsuario(req, res, next);
});

router.get('/usuario/:carteiraUsuario/obra/:hashTransacao', function(req, res, next){
	usuarioController.visualizarObra(req, res, next);
});

//router.put('/:id', controller.put);

//router.delete('/', controller.delete);

module.exports = router;